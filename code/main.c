#include "include/util.h"

int
main (int argc, char **argv)
{
  int songListPostion = 0;
  char **songs = NULL;
  char fullPath[MAX_PATH];
  musicPtr = malloc (sizeof (Music));
  flagsPtr = malloc (sizeof (Flags));
  userPathPtr = malloc (sizeof (UserPath));
  
  init_flags ();
  get_current_working_dir (argv[1], fullPath);
  songs = init_music_data ();
  find_music_folder (fullPath, musicPtr);
  if (musicPtr != NULL && musicPtr->found) {
    load_song_file_names (musicPtr->pathToMusicFolder, &songListPostion,
                          songs);
    for (int i = 0; i < songListPostion; i++) {
      printf ("%d -- song name: %s\n", i, songs[i]);
    }
  }
  
  printf ("Get busy codin, or get busy dyin.\n");
  return 0;
}
