@ECHO off

REM You will need to add vcvarsall.bat to your path in order to build this properly.
REM I would distribute the bat myself and save a ton of headache in the future, but Microsoft would be pissed lol
REM Usually, the path for vcvarsall.bat can be would via the following path:
REM C:\Program Files (x86)\Microsoft Visual Studio\2019\BuildTools\VC\Auxiliary\Build\
REM Adding it to your PATH variable will allow this script to run properly on most versions of Windows... Sorry in advance.
ECHO.
	ECHO Creating environment to build watcher in.
ECHO.

	where /q cl
	IF ERRORLEVEL 1 (
    	ECHO The application is missing. Ensure it is installed and placed in your PATH.
		CALL vcvarsall.bat x64    
	) ELSE (
    	ECHO cl exists. Let's go!
	)
ECHO.

IF EXIST ..\build\ (
	ECHO Directory: ..\build already exists.
	PUSHD ..\build
	ECHO Cleaning Directory: ..\build.
	DEL /F /Q *.*
	ECHO Done.
) ELSE (
	ECHO Generating Directory: ..\build.
	MKDIR ..\build
	PUSHD ..\build
)

	ECHO Compiling simp now...
ECHO.

REM Windows C/C++ compiler binary is 'cl' and I have no idea why... 'command line'?
REM add flags or link libraries as needed...

	CALL cl /Wall /I ..\code\include ..\code\include\util.c ..\code\main.c /Fe:simp.exe
ECHO.
  REM this is a weird bug here since it is returning both errorlevel 0 and 2...
	IF %ERRORLEVEL% EQU 0 (
		ECHO Success! simp.exe -- generated.
	) 
	IF %ERRORLEVEL% NEQ 0 (
		ECHO Failure! simp.exe -- not generated.
	)

REM Clean object file(s)
	CALL DEL *.obj

	POPD
