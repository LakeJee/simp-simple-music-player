@ECHO off
REM This script is used to format code files and clean the temps that get generated from running the indent program

ECHO.
	ECHO Formatting code in simp.

	WHERE /q indent
	IF ERRORLEVEL 1 (
		ECHO Program: indent.exe not found.
		ECHO Program: indent.exe indent can be downloaded here: http://gnuwin32.sourceforge.net/packages/indent.html
		ECHO You will need to run the setup and make sure the program is in your PATH
	) ELSE (
		ECHO Program: indent.exe exists. Let's go!
	)

ECHO.
	ECHO Formatting.
	call :formatAll
	ECHO Formatting complete.

ECHO.
  ECHO Cleaning temps.
	CALL :clean
	ECHO Cleaning temps complete.
	goto :eof


:formatAll
REM Run indent program on all code files.
for %%f in (*.c) do indent -br -ce -bli0 -cli2 -cbi0 -blf -ip2 -sai -saw -saf %%f
for /D %%d in (*) do (
    cd %%d
    call :formatAll
    cd ..
)

for %%f in (*.h) do indent %%f
for /D %%d in (*) do (
    cd %%d
    call :formatAll
    cd ..
)
GOTO :eof


:clean
REM Removes all temp generated files from calling indent
DEL /Q /F /S "*~" > NUL
GOTO :EOF