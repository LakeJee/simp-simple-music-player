#include "util.h"

void
get_current_working_dir (char *specDir, char *fullPath)
{
  char *workingDir = NULL;
  
  if (specDir == NULL) {
    if ((fullPath = _getcwd (NULL, 0)) == 
        NULL) {
      printf ("LOG::ERROR::_getcwd returned NULL\n");
    }
  } else {
    // Parse flag and set path argument
    parse_path_flag (specDir);
    
    if (userPathPtr->relative == true && strlen (userPathPtr->argument) != 0) {
      if ((workingDir = _getcwd (NULL, 0)) == NULL) {
        printf ("LOG::ERROR::_getcwd returned NULL\n");
      }
      
      StringCchCopy (fullPath, MAX_PATH, workingDir);
      StringCchCat (fullPath, MAX_PATH, TEXT ("\\"));
      StringCchCat (fullPath, MAX_PATH, userPathPtr->argument);
      
      if (_chdir (fullPath)) {
        switch (errno) {
          case ENOENT:
          printf ("Unable to locate the directory: %s\n", fullPath);
          break;
          case EINVAL:
          printf ("Invalid buffer.\n");
          break;
          default:
          printf ("Unknown error.\n");
        }
      }
      
    } else if (userPathPtr->relative == false
               && strlen (userPathPtr->argument) != 0) {
      StringCchCat (fullPath, MAX_PATH, userPathPtr->argument);
      
      if (_chdir (fullPath)) {
        switch (errno) {
          case ENOENT:
          printf ("Unable to locate the directory: %s\n", fullPath);
          break;
          case EINVAL:
          printf ("Invalid buffer.\n");
          break;
          default:
          printf ("Unknown error.\n");
        }
      }
    } else {
      printf ("doing nothing");
    }
  }
}

void
init_flags ()
{
  flagsPtr->PATH_ARG_TYPE_LENGTH = 12;
  StringCchCopyA (flagsPtr->RELATIVE_PATH, flagsPtr->PATH_ARG_TYPE_LENGTH,
                  "--relative=");
  StringCchCopyA (flagsPtr->ABSOLUTE_PATH, flagsPtr->PATH_ARG_TYPE_LENGTH,
                  "--absolute=");
}

void
parse_path_flag (char *specDir)
{
  char argType[12];
  char arg[MAX_PATH];
  
  StringCchCopyA (argType, flagsPtr->PATH_ARG_TYPE_LENGTH, specDir);
  StringCchCopyA (arg, MAX_PATH, &specDir[11]);
  
  if (strcmp (argType, flagsPtr->RELATIVE_PATH) == 0) {
    userPathPtr->relative = true;
    StringCchCopyA (userPathPtr->argument, MAX_PATH, arg);
  } else if (strcmp (argType, flagsPtr->ABSOLUTE_PATH) == 0) {
    userPathPtr->relative = false;
    StringCchCopy (userPathPtr->argument, MAX_PATH, arg);
  } else {
    printf ("unknown arg");
    userPathPtr->relative = false;
    userPathPtr->argument[0] = '\0';
  }
}

void
find_music_folder (char *workingDir, Music * ptr)
{
  if (workingDir == NULL) {
    printf ("LOG::ERROR::Current working directory is NULL\n");
    return;
  } else {
    WIN32_FIND_DATA ffd;
    TCHAR szDir[MAX_PATH];
    size_t lengthOfDir;
    HANDLE hFind = INVALID_HANDLE_VALUE;
    DWORD dwError = 0;
    
    StringCchLength (workingDir, MAX_PATH, &lengthOfDir);
    if (lengthOfDir > (MAX_PATH - 3)) {
      printf
      ("LOG::ERROR::Current working directory is too long and exceeds the max path\n");
      return;
    }
    
    StringCchCopy (szDir, MAX_PATH, workingDir);
    StringCchCat (szDir, MAX_PATH, TEXT ("\\*"));
    
    hFind = FindFirstFile (szDir, &ffd);
    
    if (INVALID_HANDLE_VALUE == hFind) {
      displayErrorBox (TEXT ("FindFirstFile"));
      return;
    }
    
    do {
      // ignore current ('.') and one directory level up ('..') and hidden directories
      if (strcmp (ffd.cFileName, ".") != 0
          && strcmp (ffd.cFileName, "..") != 0 && ffd.cFileName[0] != '.') {
        StringCchPrintfA (szDir, sizeof (szDir), "%s\\%s", workingDir,
                          ffd.cFileName);
        if (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
          if (strcmp (MUSIC_FOLDER_NAME, ffd.cFileName) == 0) {
            StringCchCopy (ptr->pathToMusicFolder, MAX_PATH, szDir);
            ptr->found = true;
            break;
          } else {
            find_music_folder ((char *) szDir, ptr);
          }
        } else if (!ptr->found) {
          // keep on do-whilin'
          continue;
        } else {
          break;
        }
      }
    } while (FindNextFile (hFind, &ffd) != 0);
    
    dwError = GetLastError ();
    if (dwError != ERROR_NO_MORE_FILES && dwError != 0) {
      displayErrorBox (TEXT ("FindFirstFile"));
    }
    
    FindClose (hFind);
  }
}


char **
init_music_data (void)
{
  char **songs = NULL;
  songs = malloc (sizeof (char *) * MAX_PATH);
  
  for (int i = 0; i < MAX_PATH; i++) {
    songs[i] = malloc (sizeof (char) * MAX_PATH);
  }
  
  return songs != NULL ? songs : NULL;
}


void
load_song_file_names (char *musicDirectory, int *songListPosition,
                      char *songs[])
{
  WIN32_FIND_DATA ffd;
  TCHAR szDir[MAX_PATH];
  size_t lengthOfDir;
  HANDLE hFind = INVALID_HANDLE_VALUE;
  DWORD dwError = 0;
  
  StringCchLength (musicDirectory, MAX_PATH, &lengthOfDir);
  if (lengthOfDir > (MAX_PATH - 3)) {
    printf
    ("LOG::ERROR::Current working directory is too long and exceeds the max path\n");
    return;
  }
  
  StringCchCopy (szDir, MAX_PATH, musicDirectory);
  StringCchCat (szDir, MAX_PATH, TEXT ("\\*"));
  
  hFind = FindFirstFile (szDir, &ffd);
  
  if (INVALID_HANDLE_VALUE == hFind) {
    displayErrorBox (TEXT ("FindFirstFile"));
    return;
  }
  
  do {
    // ignore current ('.') and one directory level up ('..') and hidden directories
    if (strcmp (ffd.cFileName, ".") != 0 && strcmp (ffd.cFileName, "..") != 0
        && ffd.cFileName[0] != '.') {
      // if we hit a directory (album folder or something) then we recurse through it
      StringCchPrintfA (szDir, sizeof (szDir), "%s\\%s", musicDirectory,
                        ffd.cFileName);
      if (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
        load_song_file_names (szDir, songListPosition, songs);
      } else {
        // we hit a song but we should check the filetype first
        if (strcmp (getFileExtension ((char *) ffd.cFileName), MP3_FILE_EXT)
            == 0
            || strlen (getFileExtension ((char *) ffd.cFileName)) >
            MP3_FILE_EXT_LENGTH) {
          songs[*songListPosition] =
            malloc (sizeof (char) * strlen (szDir) + 1);
          StringCchCopy (songs[*songListPosition], strlen (szDir) + 1, szDir);
          
          (*songListPosition)++;
        }
      }
    }
  } while (FindNextFile (hFind, &ffd) != 0);
  
  dwError = GetLastError ();
  if (dwError != ERROR_NO_MORE_FILES && dwError != 0) {
    displayErrorBox (TEXT ("FindFirstFile"));
  }
  
  FindClose (hFind);
}


char *
getFileExtension (char *fileName)
{
  size_t lengthOfFile = 0;
  StringCchLength (fileName, MAX_PATH, &lengthOfFile);
  
  if (lengthOfFile == 0 || lengthOfFile > (MAX_PATH - 3)) {
    printf
    ("LOG::ERROR::Current working directory is too long and exceeds the max path\n");
    return NULL;
  } else {
    char *e = strrchr (fileName, '.');
    if (e == NULL)
      e = "";
    return e;
  }
}


// Stole this from https://learn.microsoft.com/en-us/windows/win32/fileio/listing-the-files-in-a-directory
void
displayErrorBox (LPTSTR lpszFunction)
{
  // Retrieve the system error message for the last-error code
  LPVOID lpMsgBuf;
  LPVOID lpDisplayBuf;
  DWORD dw = GetLastError ();
  
  FormatMessage (FORMAT_MESSAGE_ALLOCATE_BUFFER |
                 FORMAT_MESSAGE_FROM_SYSTEM |
                 FORMAT_MESSAGE_IGNORE_INSERTS,
                 NULL,
                 dw,
                 MAKELANGID (LANG_NEUTRAL, SUBLANG_DEFAULT),
                 (LPTSTR) & lpMsgBuf, 0, NULL);
  
  // Display the error message and clean up
  lpDisplayBuf = (LPVOID) LocalAlloc (LMEM_ZEROINIT,
                                      (lstrlen ((LPCTSTR) lpMsgBuf) +
                                       lstrlen ((LPCTSTR) lpszFunction) +
                                       40) * sizeof (TCHAR));
  StringCchPrintf ((LPTSTR) lpDisplayBuf,
                   LocalSize (lpDisplayBuf) / sizeof (TCHAR),
                   TEXT ("%s failed with error %d: %s"), lpszFunction, dw,
                   lpMsgBuf);
  MessageBox (NULL, (LPCTSTR) lpDisplayBuf, TEXT ("Error"), MB_OK);
  
  LocalFree (lpMsgBuf);
  LocalFree (lpDisplayBuf);
}