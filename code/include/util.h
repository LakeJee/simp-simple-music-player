#pragma once

// disable annoying as fuck warings for including windows.h
#pragma warning (disable : 4668)
#pragma warning (disable : 4710)
#pragma warning (disable : 5045)
#pragma warning (disable : 4255)

#pragma comment (lib, "User32.lib")

// include all includes here because i don't give a fuuuuuck
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <windows.h>
#include <direct.h>
#include <stdbool.h>
#include <tchar.h>
#include <strsafe.h>
#include <fileapi.h>

// #defines
#define MP3_FILE_EXT          ".mp3"
#define MP3_FILE_EXT_LENGTH   5
#define FILE_SEPARATOR        "\\"
#define FILE_SEPARATOR_LENGTH 3
#define MUSIC_FOLDER_NAME     "music"

// structs
typedef struct musicBox
{
  char pathToMusicFolder[MAX_PATH];
  bool found;
} Music;
Music *musicPtr;

typedef struct flags
{
  char RELATIVE_PATH[12];
  char ABSOLUTE_PATH[12];
  size_t PATH_ARG_TYPE_LENGTH;
} Flags;
Flags *flagsPtr;

typedef struct userPath
{
  bool relative;
  char argument[MAX_PATH];
} UserPath;
UserPath *userPathPtr;


// prototypes
void get_current_working_dir (char *specDir, char *fullPath);
void find_music_folder (char *workingDir, Music * ptr);
void load_song_file_names (char *directory, int *ptr, char **songs);
char **init_music_data (void);
char *getFileExtension (char *musicDirectory);
void displayErrorBox (LPTSTR lpszFunction);
bool listDirectoryContents (const wchar_t * dir);
int getSizeOfSongList (char **songs);
void parse_path_flag (char *specDir);
void init_flags ();
