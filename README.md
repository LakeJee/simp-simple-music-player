# SIMP - Simple Music Player

This is a music player that runs in the terminal. 

I am making this because Microsft's Groove Music sucks.

## Instructions
1. Tell SIMP where to find your music using a relative (relative to where the simp executable is located) or an absolute path! (Example: `simp.exe --relative=".."` or `simp.exe --absolute="C:\\Program Files\\Documents\\Music"`)
2. Run `build.bat` to generate simp.exe.
3. TBC (to be coded)

